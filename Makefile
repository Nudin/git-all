
install:
	install git-all /usr/bin

localinstall:
	mkdir -p ${HOME}/.gitbin
	install git-all ${HOME}/.gitbin
	@echo "Add 'export PATH=\"$PATH:${HOME}/.gitbin\"' to your bashrc."

init:
	mkdir -p ${HOME}/.gitall
	echo "~/.gitall" >> ${HOME}/.gitall/repos 
	cd ${HOME}/.gitall && git init && git add repos && git commit -m "Initial commit"

