Execute git commands on a couple of repos at the same time.

# Installation
Run `make install` with root-rights to install it to `/usr/bin` or
`make localinsall` to install to the user-directory.

# Usage
git-all allows to run git commands against lists of git-repositories, that you
can specify in files in `~/.gitall/`. These files should list one path per
line. You can run `make init` to create a config in a gitrepo and add it
itself to the git-all list.

You can use `git all command` with arbitrary git commands and options. Example:

    $ git all status

Shows the status of all your managed repos.

You can have more than one repo lists in you git-all directory. Git-all will by
default use them all, but you can limit it to one of the files with `-p`.

    $ git all -p work pull

Will pull all repos listed in `~/.gitall/work`.
